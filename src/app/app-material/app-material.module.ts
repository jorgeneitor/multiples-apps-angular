/*
* Clase que contiene todos los modulos de ANGULAR MATERIAl
*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatCheckboxModule, MatRadioModule, MatCardModule, MatButtonModule, MatDialogModule, MatInputModule, MatTableModule,
  MatToolbarModule, MatMenuModule,MatIconModule, MatProgressSpinnerModule, MatButtonToggleModule, MatListModule, MatDatepickerModule,
   MatNativeDateModule, MatFormFieldModule, MatSelectModule } from '@angular/material';

// declaramos los modulos que necesitemos importar y los que queramos exportar
const imports = [CommonModule, 
                MatCheckboxModule, 
                MatRadioModule, 
                MatCardModule, MatButtonModule,
                MatDialogModule, 
                MatInputModule, 
                MatTableModule,
                MatToolbarModule, 
                MatMenuModule,
                MatIconModule, 
                MatProgressSpinnerModule,
                MatButtonToggleModule,
                MatListModule,
                MatDatepickerModule,
                MatNativeDateModule,
                MatFormFieldModule,
                MatSelectModule];

const exportModules = [CommonModule, 
                    MatCheckboxModule, 
                    MatRadioModule, 
                    MatCardModule, MatButtonModule,
                    MatDialogModule, 
                    MatInputModule, 
                    MatTableModule,
                    MatToolbarModule, 
                    MatMenuModule,
                    MatIconModule, 
                    MatProgressSpinnerModule,
                    MatButtonToggleModule,
                    MatListModule,
                    MatDatepickerModule,
                    MatNativeDateModule,
                    MatFormFieldModule,
                    MatSelectModule];

@NgModule({
  declarations: [],
  imports: imports,
  exports: exportModules
})
export class AppMaterialModule { }
