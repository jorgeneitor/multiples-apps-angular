import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component'

const routes: Routes =  [
  {
    path: 'app1',
    loadChildren: () =>
      import('../../projects/app1/src/app/app.module').then(m => m.AppModule),
  },
  {
    path: 'app2',
    loadChildren: () =>
      import('../../projects/app2/src/app/app.module').then(m => m.AppModule),
  },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: 'login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
