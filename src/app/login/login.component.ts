import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';
import { GlobalVariablesService } from '../common/services/global-variables.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    constructor(private router: Router, private globalVariables: GlobalVariablesService) { }

    username: string;
    password: string;

    ngOnInit() {
    }
    
    login() : void {
      if(this.username == 'admin' && this.password == 'admin') {
        this.router.navigate(["/app1"]);
        this.globalVariables.logged = true;
        this.globalVariables.showApp1 = true;
        this.globalVariables.showApp2 = true;
      } else if (this.username == 'jorge' && this.password == 'jorge') {
        this.router.navigate(["/app1"]);
        this.globalVariables.logged = true;
        this.globalVariables.showApp1 = true;
        this.globalVariables.showApp2 = false;
      } else if (this.username == 'jorge2' && this.password == 'jorge') {
        this.router.navigate(["/app2"]);
        this.globalVariables.logged = true;
        this.globalVariables.showApp1 = false;
        this.globalVariables.showApp2 = true;
      }
    }
}
