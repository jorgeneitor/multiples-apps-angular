import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { AppMaterialModule } from './app-material/app-material.module';
import { LoginComponent } from './login/login.component'
import { GlobalVariablesService } from './common/services/global-variables.service';

@NgModule({
  declarations: [AppComponent, NavComponent, LoginComponent],
  imports: [BrowserModule, FormsModule, AppRoutingModule, AppMaterialModule, BrowserAnimationsModule],
  providers: [GlobalVariablesService],
  bootstrap: [AppComponent],
})
export class AppModule {}
