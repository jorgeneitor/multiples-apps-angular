import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { GlobalVariablesService } from '../common/services/global-variables.service'

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor(private router: Router, private globalVariables: GlobalVariablesService) { }

  public showApp1 :boolean;
  public showApp2 :boolean;
  ngOnInit() {
    this.showApp1 = this.globalVariables.showApp1;
    this.showApp2 = this.globalVariables.showApp2;
  }


  logout() :void {
    this.router.navigate(["login"]);
  }

}
