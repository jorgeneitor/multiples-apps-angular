import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-view1',
  templateUrl: './view1.component.html',
  styleUrls: ['./view1.component.scss']
})
export class View1Component implements OnInit {

  title : any;
  constructor() { }

  ngOnInit() {
    this.title = 'app2 componente 1' +  _.random(0, 50000);
  }

}
