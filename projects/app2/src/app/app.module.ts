import { NgModule, ModuleWithProviders } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { View1Component } from './view1/view1.component';
import { View2Component } from './view2/view2.component';
import { NavComponent } from './nav/nav.component';
import { CommonModule } from '@angular/common';

import { AppMaterialModule } from '../../../../src/app/app-material/app-material.module';

@NgModule({
  declarations: [AppComponent, View1Component, View2Component, NavComponent],
  imports: [CommonModule, AppRoutingModule, AppMaterialModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
