import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-view1',
  templateUrl: './view1.component.html',
  styleUrls: ['./view1.component.scss']
})
export class View1Component implements OnInit {

  title : Number;

  constructor() {
    
   }

  ngOnInit() {
    this.title = _.random(0, 50000);
  }

}
